package com.parksideexercise.listeners;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.Reporter;

public class RetryAnalyzer implements IRetryAnalyzer {

	private int retryCount = 0;
	private static final int maxRetryCount = 3;
	
	@Override
	public boolean retry(ITestResult result) {

		if (retryCount < maxRetryCount) {
			retryCount++;
			Reporter.log("Retry #" + retryCount + " for test: " + result.getMethod().getMethodName() + ", on thread: "
					+ Thread.currentThread().getName(), true);
			return true;
		}
		return false;
	}
}
