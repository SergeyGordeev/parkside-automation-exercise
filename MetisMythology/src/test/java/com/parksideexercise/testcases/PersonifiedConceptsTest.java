package com.parksideexercise.testcases;

import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.Reporter;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.parksideexercise.base.TestBase;
import com.parksideexercise.utilities.*;

import org.openqa.selenium.interactions.Actions;

public class PersonifiedConceptsTest extends TestBase {

	JavascriptExecutor js = (JavascriptExecutor) driver;

	@Test(priority = 1, dataProviderClass = DataUtil.class, dataProvider = "data")
	public void personifiedConceptsToolTipValidation(Hashtable<String, String> data) throws InterruptedException {

		// need move to TestBase
		JavascriptExecutor js = (JavascriptExecutor) driver;

		System.out.println("***************************************************************************");
		Reporter.log("Test Case Number: " + data.get("TC_ID") + " | " + "Test Module: " + data.get("Module") + " | "
				+ "Test Description: " + data.get("Description"), true);
		System.out.println("---------------------------------------------------------------------------");

		Reporter.log("---Preconditions---" + data.get("TC_ID"), true);
		String BaseURL = data.get("BaseURL");
		Reporter.log("BaseURL:" + BaseURL, true);
		String TitleBasePage = data.get("TitleBasePage");
		Reporter.log("TitleBasePage: " + TitleBasePage, true);
		String ExpectedTEXT = data.get("ExpectedTEXT");
		Reporter.log("ExpectedTEXT: " + ExpectedTEXT, true);
		String xpathLINK = data.get("xpathLINK");
		Reporter.log("xpathLINK: " + xpathLINK, true);
		String xpathLINKTooltipText = data.get("xpathLINKTooltipText");
		Reporter.log("xpathLINKTooltipText: " + xpathLINKTooltipText, true);

		// Home Page validation
		// ************************
		Reporter.log("---TEST---" + data.get("TC_ID"), true);
		// Validating Home Page is displayed Current URL equal Expected
		Assert.assertEquals(driver.getCurrentUrl(), OR.getProperty(BaseURL),
				"ERROR: Home Page is NOT present --> Current URL is NOT equal Expected");
		Reporter.log("HOME PAGE CURRENT URL VALIDATION DONE", true);

		// Validating Home Page is displayed Current Title Home Page equal Expected
		Assert.assertEquals(driver.getTitle(), OR.getProperty(TitleBasePage),
				"ERROR: Home Page is NOT present --> Current TitleBasePage is NOT equal Expected");
		Reporter.log("HOME PAGE CURRENT TITLE VALIDATION DONE", true);

		// Validating Home Page is displayed 'firstHeading' Present
		Assert.assertTrue(driver.findElement(By.xpath(OR.getProperty("FirstHeading"))).isDisplayed(),
				"ERROR: Home Page is NOT present --> First Heading Metis is NOT Displayed");
		Reporter.log("HOME PAGE FIRST HEADING VALIDATION DONE", true);

		Thread.sleep(1000);

		// Scroll vertically down by 800 pixels
		js.executeScript("window.scrollBy(0,800)");

		// Instantiate Action Class
		Actions actions = new Actions(driver);
		// Retrieve WebElement 'PConcepts' to perform mouse hover
		WebElement PConcepts = driver.findElement(By.xpath(OR.getProperty(xpathLINK)));

		// Mouse hover on link
		actions.moveToElement(PConcepts).perform();
		Thread.sleep(1000);

		String actualText = driver.findElement(By.xpath(OR.getProperty(xpathLINKTooltipText))).getText();
		Assert.assertEquals(actualText, ExpectedTEXT, "ERROR: Personified concepts doesn't contain desired value");
		Reporter.log("'PERSONIFIED CONCEPTS' TOOL TIP TEXT VALIDATION - DONE", true);

		Thread.sleep(1000);

		// Scroll vertically UP by 800 pixels
		js.executeScript("window.scrollBy(0,-800)");

		Reporter.log(data.get("QA NOTE"), true);
		Reporter.log("---TEST END---" + data.get("TC_ID"), true);

//		Reporter.log(driver.findElement(By.xpath(OR.getProperty(xpathLINKTooltipText))).getText(), true);

	}

	@Test(priority = 2, dataProviderClass = DataUtil.class, dataProvider = "data")
	public void personifiedConceptsBoxLinksValidation(Hashtable<String, String> data)
			throws InterruptedException, MalformedURLException, IOException {

		System.out.println("***************************************************************************");
		Reporter.log("Test Case Number: " + data.get("TC_ID") + " | " + "Test Module: " + data.get("Module") + " | "
				+ "Test Description: " + data.get("Description"), true);
		System.out.println("---------------------------------------------------------------------------");

		Reporter.log("---Preconditions---" + data.get("TC_ID"), true);
		String BaseURL = data.get("BaseURL");
		Reporter.log("BaseURL:" + BaseURL, true);
		String TitleBasePage = data.get("TitleBasePage");
		Reporter.log("TitleBasePage: " + TitleBasePage, true);
		String TitleRederectPage = data.get("TitleRederectPage");
		Reporter.log("TitleRederectPage: " + TitleRederectPage, true);
		String ExpectedURL = data.get("ExpectedURL");
		Reporter.log("ExpectedURL: " + ExpectedURL, true);
		String xpathLINK = data.get("xpathLINK");
		Reporter.log("xpathLINK: " + xpathLINK, true);
		String FalmilyTreeHeadingXPATH = data.get("FalmilyTreeHeadingXPATH");
		Reporter.log("FalmilyTreeHeadingXPATH: " + FalmilyTreeHeadingXPATH);

		String ExpectedHomePageFirstHeadingText = data.get("ExpectedHomePageFirstHeadingText");
		System.out.println("ExpectedHomePageFirstHeadingText: " + ExpectedHomePageFirstHeadingText);

		// Home Page validation
		// ************************
		Reporter.log("---TEST---" + data.get("TC_ID"), true);
		// Validating Home Page is displayed Current URL equal Expected
		Assert.assertEquals(driver.getCurrentUrl(), OR.getProperty(BaseURL),
				"ERROR: Home Page is NOT present --> Current URL is NOT equal Expected");
		Reporter.log("HOME PAGE CURRENT URL VALIDATION DONE", true);

		// Validating Home Page is displayed Current Title Home Page equal Expected
		Assert.assertEquals(driver.getTitle(), OR.getProperty(TitleBasePage),
				"ERROR: Home Page is NOT present --> Current TitleBasePage is NOT equal Expected");
		Reporter.log("HOME PAGE CURRENT TITLE VALIDATION DONE", true);

		// Validating Home Page is displayed 'firstHeading' Present
		Assert.assertTrue(driver.findElement(By.xpath(OR.getProperty("FirstHeading"))).isDisplayed(),
				"ERROR: Home Page is NOT present --> First Heading Metis is NOT Displayed");
		Reporter.log("HOME PAGE FIRST HEADING VALIDATION DONE", true);

		Thread.sleep(1000);

		// `Personified Concepts` box links validation
		// ************************

		// Validation the WebElement Link on `Personified Concepts` box is Enabled
		Assert.assertTrue(driver.findElement(By.xpath(OR.getProperty(xpathLINK))).isEnabled(),
				"ERROR: Link is not Enabled or Part is not executed");
		Reporter.log("'PERSONIFIED CONCEPTS' BOX WEB ELEMENT IS ENABLED LINK VALIDATION DONE", true);

		// Validation the WebElement Link on `Personified Concepts` box is Displayed
		Assert.assertTrue(driver.findElement(By.xpath(OR.getProperty(xpathLINK))).isDisplayed(),
				"ERROR: Link is not Displayed or Part is not executed");
		Reporter.log("'PERSONIFIED CONCEPTS' BOX WEB ELEMENT IS DISPLAYED LINK VALIDATION DONE", true);

		// Validation WebElement 'Personified Concepts' link with equal to Expected link
		String ActualLinkText = driver.findElement(By.xpath(OR.getProperty(xpathLINK))).getAttribute("href");
		Assert.assertEquals(ActualLinkText, OR.getProperty(ExpectedURL),
				"ERROR: Link URL is not matching or Part is not executed");
		Reporter.log("'PERSONIFIED CONCEPTS' BOX WEB ELEMENT LINK EQUAL TO EXPECTED VALIDATION DONE", true);

		// VALIDATION 'PERSONIFIED CONCEPTS' BOX LINK NAVIGATED, REDIRECTED PAGE URL
		WebElement link = driver.findElement(By.xpath(OR.getProperty(xpathLINK)));
		link.click();
		String ActualURL = driver.getCurrentUrl();
		Assert.assertEquals(ActualURL, OR.getProperty(ExpectedURL),
				"ERROR: The Link is not rederected on desired page");
		Reporter.log("'PERSONIFIED CONCEPTS' BOX LINK NAVIGATED, REDIRECTED PAGE URL VALIDATION DONE", true);

		System.out.println(driver.getTitle());

		// VALDIATION 'PERSONIFIED CONCEPTS' BOX LINK NAVIGATED, REDIRECTED PAGE TITLE
		String ActualRedirectedTitle = driver.getTitle();
		Assert.assertEquals(ActualRedirectedTitle, OR.getProperty(TitleRederectPage),
				"ERROR: The Link is not rederected on desired page");
		Reporter.log("'PERSONIFIED CONCEPTS' BOX LINK NAVIGATED, REDIRECTED PAGE TITLE VALIDATION DONE", true);

		// IF NIKE PERSONIFIED CONCEPTS' BOX LINK NAVIGATED, REDIRECTED PAGE DISPLAYS A
		// 'FAMILY TREE' HEADING TEXT
		if (ActualRedirectedTitle.contains("Nike")) {
			WebElement FamilyTree = driver.findElement(By.xpath("//*[@id=\"Family_tree\"]"));
			String actualFamilyTreeString = FamilyTree.getText();
			Assert.assertEquals(actualFamilyTreeString, "Family tree",
					"ERROR: The Link is not rederected on desired WebElement Text");
			Reporter.log(
					"'PERSONIFIED CONCEPTS' BOX LINK NAVIGATED, REDIRECTED PAGE DISPLAYS A 'FAMILY TREE' HEADING TEXT VALIDATION DONE",
					true);
		}

		Thread.sleep(1000);

		driver.navigate().back();

		/*
		 * An object of the class HttpURLConnection is created to get the HTTP response
		 * code. To establish a link to an URL, the openConnection method shall be used.
		 * Next, we shall utilize the setRequestMethod and pass HEAD as a parameter. For
		 * connection, the connect method is to be applied to the instance of the
		 * HttpURLConnection class. At last, the getResponseCode method gets the
		 * response code
		 */
		// Validating if tested link is not broken
		HttpURLConnection c = (HttpURLConnection) new URL(ActualURL).openConnection();
		c.setRequestMethod("HEAD");
		c.connect();
		int r = c.getResponseCode();

		// Validating if tested link is not broken
		Assert.assertEquals(r, 200, "ERROR: Link response code is not 200");
		Reporter.log("'PERSONIFIED CONCEPTS' LINK CONTAIN CORRECT RESPONSE CODE VALIDATION DONE, CODE IS: " + r, true);

		Reporter.log(data.get("QA NOTE"), true);
		Reporter.log("---TEST END---" + data.get("TC_ID"), true);

	}

}