package com.parksideexercise.testcases;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.Reporter;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.parksideexercise.base.TestBase;
import com.parksideexercise.utilities.DataUtil;

public class PageHealthTest extends TestBase {

	@Test(priority = 1, dataProviderClass = DataUtil.class, dataProvider = "data")
	public void brockenImagesTest(Hashtable<String, String> data) {

		System.out.println("***************************************************************************");
		Reporter.log("Test Case Number: " + data.get("TC_ID") + " | " + "Test Module: " + data.get("Module") + " | "
				+ "Test Description: " + data.get("Description"), true);
		System.out.println("---------------------------------------------------------------------------");

		Reporter.log("---Preconditions---" + data.get("TC_ID"), true);
		Reporter.log("Page under test 'Title': " + driver.getTitle(), true);
		Reporter.log("Page under test 'URL': " + driver.getCurrentUrl(), true);

		Reporter.log("---TEST---" + data.get("TC_ID"), true);
		List<WebElement> images = driver.findElements(By.tagName("img"));
		System.out.println("TOTAL NUMBER IMAGES ON THE PAGE: " + images.size());

		for (WebElement ele : images) {
			try {
				HttpURLConnection conn = (HttpURLConnection) new URL(ele.getAttribute("src")).openConnection();
				conn.setRequestMethod("GET");
				int responceCode = conn.getResponseCode();

				if (responceCode != 200) {
					System.err.println("BROKEN IMAGE: " + ele.getAttribute("src"));
				} else {
					System.out.println("FINE IMAGE: " + ele.getAttribute("src"));
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		Reporter.log(data.get("QA NOTE"), true);
		Reporter.log("---TEST END---" + data.get("TC_ID"), true);
	}

//	@Test(priority = 2, dataProviderClass = DataUtil.class, dataProvider = "data")
//	public void brockenLinksTest(Hashtable<String, String> data) throws Exception {
//
//		System.out.println("***************************************************************************");
//		Reporter.log("Test Case Number: " + data.get("TC_ID") + " | " + "Test Module: " + data.get("Module") + " | "
//				+ "Test Description: " + data.get("Description"), true);
//		System.out.println("---------------------------------------------------------------------------");
//
//		Reporter.log("---Preconditions---" + data.get("TC_ID"), true);
//		Reporter.log("Page under test 'Title': " + driver.getTitle(), true);
//		Reporter.log("Page under test 'URL': " + driver.getCurrentUrl(), true);
//
//		Reporter.log("---TEST---" + data.get("TC_ID"), true);
//		Reporter.log(data.get("QA NOTE"), true);
//
//		/*
//		 * Used tagName method to collect the list of items with tagName "a"
//		 * findElements - to find all the elements with in the current page. It returns
//		 * a list of all web elements or an empty list if nothing matches
//		 */
//		List<WebElement> links = driver.findElements(By.tagName("a"));
//		// To print the total number of links
//		System.out.println("Total links are " + links.size());
//		// used for loop to
//		for (int i = 0; i < links.size(); i++) {
//			WebElement element = links.get(i);
//			// By using "href" attribute, we could get the url of the requried link
//			String url = element.getAttribute("href");
//
//			// calling verifyLink() method here. Passing the parameter as url which we
//			// collected in the above link
//			// See the detailed functionality of the verifyLink(url) method below
//
//			/*
//			 * Calling verifyLink() method here. Passing the parameter as url which we
//			 * collected in the above link See the detailed functionality of the
//			 * verifyLink(url) method below
//			 */
//			verifyLink(url);
//
//		}
//	}
//
//	/*
//	 * The below function verifyLink(String urlLink) verifies any broken links and
//	 * return the server status.
//	 */
//	public static void verifyLink(String urlLink) {
//
//		/*
//		 * Sometimes we may face exception "java.net.MalformedURLException". Keep the
//		 * code in try catch block to continue the broken link analysis
//		 */
//		try {
//			// Use URL Class - Create object of the URL Class and pass the urlLink as
//			// parameter
//			URL link = new URL(urlLink);
//			// Create a connection using URL object (i.e., link)
//			HttpURLConnection httpConn = (HttpURLConnection) link.openConnection();
//			// Set the timeout for 2 seconds
//			httpConn.setConnectTimeout(2000);
//			// connect using connect method
//			httpConn.connect();
//			// use getResponseCode() to get the response code.
//			if (httpConn.getResponseCode() > 200) {
//				Reporter.log(urlLink + " - " + httpConn.getResponseCode() + " : " + httpConn.getResponseMessage(),
//						true);
//
//			}
//
//		}
//
//		/*
//		 * getResponseCode method returns = IOException - if an error occurred
//		 * connecting to the server.
//		 */
//		catch (Exception e) {
//			// e.printStackTrace();
//		}
//
//	}
}
