package com.parksideexercise.testcases;

import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.Reporter;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.parksideexercise.base.TestBase;
import com.parksideexercise.utilities.*;

public class ContentsBoxTest extends TestBase {

	@Test(priority = 1, dataProviderClass = DataUtil.class, dataProvider = "data")
	public void contentsBoxHaveFunctioningHyperlinks(Hashtable<String, String> data)
			throws InterruptedException, Exception {

		System.out.println("***************************************************************************");
		Reporter.log("Test Case Number: " + data.get("TC_ID") + " | " + "Test Module: " + data.get("Module") + " | "
				+ "Test Description: " + data.get("Description"), true);
		System.out.println("---------------------------------------------------------------------------");

		Reporter.log("---Preconditions---" + data.get("TC_ID"), true);
		String BaseURL = data.get("BaseURL");
		Reporter.log("BaseURL:" + BaseURL, true);

		String TitleBasePage = data.get("TitleBasePage");
		Reporter.log("TitleBasePage: " + TitleBasePage, true);

		String ExpectedURL = data.get("ExpectedURL");
		Reporter.log("ExpectedURL: " + ExpectedURL, true);

		String xpathLINK = data.get("xpathLINK");
		Reporter.log("xpathLINK: " + xpathLINK, true);

		// Home Page validation
		// ************************
		Reporter.log("---TEST---" + data.get("TC_ID"), true);
		// Validating Home Page is displayed Current URL equal Expected
		Assert.assertEquals(driver.getCurrentUrl(), OR.getProperty(BaseURL),
				"ERROR: Home Page is NOT present --> Current URL is NOT equal Expected");
		Reporter.log("HOME PAGE CURRENT URL VALIDATION DONE", true);

		// Validating Home Page is displayed Current Title Home Page equal Expected
		Assert.assertEquals(driver.getTitle(), OR.getProperty(TitleBasePage),
				"ERROR: Home Page is NOT present --> Current TitleBasePage is NOT equal Expected");
		Reporter.log("HOME PAGE CURRENT TITLE VALIDATION DONE", true);

		// Validating Home Page is displayed 'firstHeading' Present
		Assert.assertTrue(driver.findElement(By.xpath(OR.getProperty("FirstHeading"))).isDisplayed(),
				"ERROR: Home Page is NOT present --> First Heading Metis is NOT Displayed");
		Reporter.log("HOME PAGE FIRST HEADING VALIDATION DONE", true);

		// `Contents` box links validation
		// ************************

		// Validation `Contents` box are used as headings on the page
		Assert.assertTrue(TestUtil.IsElementPresent() == true,
				"ERROR: Element not found, `Contents` box are not used as headings on the page");
		Reporter.log("'CONTENTS' BOX USED HEDING ON PAGE VALIDATION DONE", true);

		// Validation the WebElement Link on `Contents` box is Enabled
		Assert.assertTrue(driver.findElement(By.xpath(OR.getProperty(xpathLINK))).isEnabled(),
				"ERROR: Link is not Enabled or Part is not executed");
		Reporter.log("'CONTENTS' BOX WEB ELEMENT IS ENABLED LINK VALIDATION DONE", true);

		// Validation the WebElement Link on `Contents` box is Displayed
		Assert.assertTrue(driver.findElement(By.xpath(OR.getProperty(xpathLINK))).isDisplayed(),
				"ERROR: Link is not Displayed or Part is not executed");
		Reporter.log("'CONTENTS' BOX WEB ELEMENT IS DISPLAYED LINK VALIDATION DONE", true);

		// Validation WebElement contains link with equal to Expected link
		String ActualLinkText = driver.findElement(By.xpath(OR.getProperty(xpathLINK))).getAttribute("href");
		Assert.assertEquals(ActualLinkText, OR.getProperty(ExpectedURL),
				"ERROR: Link URL is not matching or Part is not executed");
		Reporter.log("'CONTENTS' BOX WEB ELEMENT LINK EQUAL TO EXPECTED VALIDATION DONE", true);

		// Validation if User move to an other page or page segment after clicking on
		// link
		WebElement link = driver.findElement(By.xpath(OR.getProperty(xpathLINK)));
		link.click();
		String ActualURL = driver.getCurrentUrl();
		Assert.assertEquals(ActualURL, OR.getProperty(ExpectedURL),
				"ERROR: The Link is not rederected on desired page");
		Reporter.log("'CONTENTS' BOX LINK NAVIGATED FUNCTIONAL VALIDATION DONE", true);

		driver.navigate().back();

		/*
		 * An object of the class HttpURLConnection is created to get the HTTP response
		 * code. To establish a link to an URL, the openConnection method shall be used.
		 * Next, we shall utilize the setRequestMethod and pass HEAD as a parameter. For
		 * connection, the connect method is to be applied to the instance of the
		 * HttpURLConnection class. At last, the getResponseCode method gets the
		 * response code
		 */
		// Validating if tested link is not broken
		HttpURLConnection c = (HttpURLConnection) new URL(ActualURL).openConnection();
		c.setRequestMethod("HEAD");
		c.connect();
		int r = c.getResponseCode();

		// Validating if tested link is not broken
		Assert.assertEquals(r, 200, "ERROR: Link response code is not 200");
		Reporter.log("'CONTENTS' LINK CONTAIN CORRECT RESPONSE CODE VALIDATION DONE, CODE IS: " + r, true);

		Reporter.log(data.get("QA NOTE"), true);
		Reporter.log("---TEST END---" + data.get("TC_ID"), true);

	}

}
