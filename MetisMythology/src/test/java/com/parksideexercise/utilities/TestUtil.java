package com.parksideexercise.utilities;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Hashtable;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.DataProvider;

import com.parksideexercise.base.TestBase;

public class TestUtil extends TestBase {

	public static String screenshotPath;
	public static String screenshotName;

	public static void captureScreenshot() throws IOException {

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		Date d = new Date();
		screenshotName = d.toString().replace(":", "_").replace(" ", "_") + ".jpg";

		FileUtils.copyFile(scrFile,
				new File(System.getProperty("user.dir") + "/target/surefire-reports/html/" + screenshotName));
		FileUtils.copyFile(scrFile, new File("./reports/" + screenshotName));

	}

	@DataProvider(name = "dp")
	public Object[][] getData(Method m) {

		String sheetName = m.getName();
		int rows = excel.getRowCount(sheetName);
		int cols = excel.getColumnCount(sheetName);

		Object[][] data = new Object[rows - 1][1];

		Hashtable<String, String> table = null;

		for (int rowNum = 2; rowNum <= rows; rowNum++) { // 2

			table = new Hashtable<String, String>();

			for (int colNum = 0; colNum < cols; colNum++) {

				// data[0][0]
				table.put(excel.getCellData(sheetName, colNum, 1), excel.getCellData(sheetName, colNum, rowNum));
				data[rowNum - 2][0] = table;
			}

		}

		return data;

	}

	public static boolean isTestRunnable(String testName, ExcelReader excel) {

		String sheetName = "test_suite";
		int rows = excel.getRowCount(sheetName);

		for (int rNum = 2; rNum <= rows; rNum++) {

			String testCase = excel.getCellData(sheetName, "TCID", rNum);

			if (testCase.equalsIgnoreCase(testName)) {

				String runmode = excel.getCellData(sheetName, "Runmode", rNum);

				if (runmode.equalsIgnoreCase("Y"))
					return true;
				else
					return false;
			}

		}
		return false;
	}

	/*
	 * The user has to handle the Element Not Found execution, which occurred during script execution.
     * The Try an catch block to be used to handle the above exception. So the Script will not stop execution
     * but proceed with the Next steps.
	 */
	public static boolean IsElementPresent() {
		try {
			
			if (driver.findElements(By.xpath(OR.getProperty("Contents_h2_XPATH"))).size() != 0) {

			}
			return true;
		} catch (Exception e) {
			System.out.println("Exception occured Element Not found occured" + e);
			return false;

		}

	}
	
//	/*
//	 * An object of the class HttpURLConnection is created to get the HTTP response code. 
//	 * To establish a link to an URL, the openConnection method shall be used. Next, 
//	 * we shall utilize the setRequestMethod and pass HEAD as a parameter.
//	 * For connection, the connect method is to be applied to the instance of the HttpURLConnection class. 
//	 * At last, the getResponseCode method gets the response code
//	 */
//	public static boolean singleLinkValidation() {
//		
//		HttpURLConnection c = (HttpURLConnection) new URL(ExpectedURL).openConnection();
//		// set the HEAD request with setRequestMethod
//		c.setRequestMethod("HEAD");
//		// connection started and get response code
//		c.connect();
//		int r = c.getResponseCode();
//		System.out.println("Http response code: " + r);
//		
//		
//	}
	
	

}
